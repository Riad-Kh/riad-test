<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\DoctorLoginRequest;
use App\services\AuthServices;
use App\services\DoctorServices;
use Illuminate\Http\Request;

/**
 * Class DoctorController
 */
class DoctorController extends Controller
{


    /**
     * @OA\Post(path="/doctor/login",
     *     tags={"Doctor"},
     *     summary="Login as doctor",
     *     operationId="authDocLogin",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginModel")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "User Profile response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param DoctorLoginRequest $request
     * @return JsonResponse
     */
    public function login(DoctorLoginRequest $request)
    {
        return DoctorServices::login($request);
    }

    /**
     * @OA\Patch(path="/doctor/accept-reject",
     *     tags={"Doctor"},
     *     summary="Accept Reject Appotment",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="appotment id to cancel It",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         in="query",
     *         name="status",
     *         required=true,
     *         description=" 2 => Accept , 3 =>Reject",
     *              schema={"type": "integer",
     *                       "enum": {"2","3"},
     *                       }
     *       ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

     public function accept_reject(Request $request)
     {
         return DoctorServices::accept_reject($request);
     }


}
