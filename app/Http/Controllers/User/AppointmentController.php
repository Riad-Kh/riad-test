<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateAppointmentRequest;

use App\services\AdminAppointmentServices;
use App\services\UserServices;
use Illuminate\Http\Request ;

class AppointmentController extends Controller
{


    /**
     * @OA\post(path="/user/appointment/create",
     *      tags={"User"},
     *     summary="create an appointment",
     *     security={{"apiAuth":{}}},
     *     operationId="Crerateuserappointment",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/UserCrearteAppointmentRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param CreateAppointmentRequest $request
     * @return string
     */

     public function create_appointment(CreateAppointmentRequest $request)
     {
        return UserServices::create($request);
     }



    /**
     * @OA\Get(path="/user/appointment/my_appointment",
     *      tags={"User"},
     *     summary="Get appointments",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function my_appointments()
    {
        return UserServices::myAppointments();
    }

    /**
     * @OA\Patch(path="/user/appointment/cancel",
     *      tags={"User"},
     *     summary="Get appointments",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="appotment id to cancel It",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

     public function cancel(Request $request)
     {
         return UserServices::cancel($request);
     }



}
