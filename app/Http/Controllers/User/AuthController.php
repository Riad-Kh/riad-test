<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\UpdateProfileRequest;
use App\services\AuthServices;
use Illuminate\Http\Request;

/**
 * Class AuthController
 */
class AuthController extends Controller
{

    /**
     * @OA\Post(path="/user-register",
     *     tags={"Auth"},
     *     summary="Register as a new user",
     *     operationId="authRegister",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Registration model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/RegisterUserModel")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param RegisterRequest $request
     * @return string
     */

    public function user_register(RegisterRequest $request)
    {

        list($res, $data, $msg , $ex) = AuthServices::User_Register($request);

        if($res){
            return response()->json($data);
        } else {
            return returnError($msg , $ex);
        }
    }

    /**
     * @OA\Post(path="/user-login",
     *     tags={"Auth"},
     *     summary="Login as a user",
     *     operationId="authLogin",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginModel")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "User Profile response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function user_login(LoginRequest $request)
    {
        list($res, $data, $msg, $ex) = AuthServices::User_Login($request);

        if ($res) {
            return response()->json($data);
        } else {
            return returnError($msg, $ex);
        }
    }

    /**
     * @OA\Post(path="/logout",
     *     tags={"Auth"},
     *     summary="Log out from system",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        return AuthServices::User_logut($request);
    }

      /**
     * @OA\Post(path="/update-profile",
     *     tags={"Auth"},
     *     summary="Edit profile content",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *              @OA\Schema(ref="#components/schemas/EditProfileRequest"),
     *        )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResultProfileResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param UpdateProfileRequest $request
     * @return JsonResponse
     */

    public function update_profile(UpdateProfileRequest $request)
    {

        return AuthServices::Update_Profile($request);
    }


    /**
     * @OA\Get(path="/profile",
     *     tags={"Auth"},
     *     summary="Get profile details",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function profile()
    {
        list($res, $data, $msg, $ex) = AuthServices::User_Profile();

        if ($res) {
            return response()->json($data);
        } else {
            return returnError($msg, $ex);
        }
    }

    /**
     * @OA\Delete(path="/delete-account",
     *     tags={"Auth"},
     *     summary="Delete as a user",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @return JsonResponse
     */

    public function delete_account(Request $request)
    {
        return AuthServices::DeleteAccount($request);
    }

    /**
     * @OA\Post(path="/change-password",
     *     tags={"Auth"},
     *     summary="Edit profile password",
     *     security={{"apiAuth":{}}},
     *     @OA\RequestBody(
     *         description="ChangePasswordPayload model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ChangePasswordPayload")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     */
    public function change_password(ChangePasswordRequest $request)
    {
        return AuthServices::UserChangePassword($request);
    }

     /**
     * @OA\Get(path="/check-token",
     *     tags={"Auth"},
     *     summary="Test a token for expiration",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function check_token()
    {
        return AuthServices::checkToken();

    }

}
