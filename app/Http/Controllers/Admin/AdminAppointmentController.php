<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Appointment\CreateAppointmentRequest;
use App\Http\Requests\Appointment\GetAppointmentRequest;
use App\Http\Requests\Appointment\UpdateAppointmentRequest;
use App\Http\Requests\User\GetUserRequest;
use App\services\AdminAppointmentServices;
use Illuminate\Http\Request;

class AdminAppointmentController extends Controller
{


    /**
     * @OA\post(path="/admin/appointment/create",
     *      tags={"Appointment CRUD"},
     *     summary="create an appointment",
     *     security={{"apiAuth":{}}},
     *     operationId="Crerateappointment",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CrearteAppointmentRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param CreateAppointmentRequest $request
     * @return string
     */

     public function create_appointment(CreateAppointmentRequest $request)
     {
        return AdminAppointmentServices::createOrUpdate($request);
     }


    /**
     * @OA\post(path="/admin/appointment/update",
     *      tags={"Appointment CRUD"},
     *     summary="Update an appointment",
     *     security={{"apiAuth":{}}},
     *     operationId="Updateappointment",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="appointment id to update It",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CrearteAppointmentRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param UpdateAppointmentRequest $request
     * @return string
     */

    public function update_appointment(UpdateAppointmentRequest $request)
    {
       return AdminAppointmentServices::createOrUpdate($request);
    }



    /**
     * @OA\Get(path="/admin/appointment/get-all",
     *      tags={"Appointment CRUD"},
     *     summary="Get appointments",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function get_appointments()
    {
        return AdminAppointmentServices::getAppointments();
    }



    /**
     * @OA\Get(path="/admin/appointment/get-one",
     *      tags={"Appointment CRUD"},
     *     summary="Get appointment",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="appointment_id",
     *         in="query",
     *         description="appointment Id To Show It",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function get_appointment(GetAppointmentRequest $request)
    {
        return AdminAppointmentServices::getAppointment($request);

    }

    /**
     * @OA\Delete(path="/admin/appointment/delete",
     *      tags={"Appointment CRUD"},
     *     summary="delete an appointment",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="appointment_id",
     *         in="query",
     *         description="appointment Id To Delete It",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param GetAppointmentRequest $request
     * @return JsonResponse
     */

    public function delete_appointment(GetAppointmentRequest $request)
    {
        return AdminAppointmentServices::deleteAppointment($request);
    }

    /**
     * @OA\Get(path="/admin/appointment/user-appointments",
     *      tags={"Appointment CRUD"},
     *     summary="Get user  appointment",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="user Id To Show Appointmens",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

     public function user_appointments(GetUserRequest $request)
     {
         return AdminAppointmentServices::getUserAppointments($request);
     }


    /**
     * @OA\post(path="/admin/appointment/filters",
     *      tags={"Appointment CRUD"},
     *     summary="filters",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/FilterRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param CreateAppointmentRequest $request
     * @return string
     */

     public function filters(Request $request)
     {
        return AdminAppointmentServices::filters($request);
     }
}
