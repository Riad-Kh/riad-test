<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Doctor\CreateDoctorRequest;
use App\Http\Requests\Doctor\GetDoctorRequest;
use App\Http\Requests\Doctor\UpdateDoctorRequest;

use App\services\AdminDoctorServices;


class AdminDoctorController extends Controller
{


    /**
     * @OA\post(path="/admin/doctor/create",
     *     tags={"Doctor CRUD"},
     *     summary="create a doctor",
     *     security={{"apiAuth":{}}},
     *     operationId="Creratedoctor",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CrearteDoctorByAdminRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param CreateDoctorRequest $request
     * @return string
     */

     public function create_doctor(CreateDoctorRequest $request)
     {
        return AdminDoctorServices::createOrUpdate($request);
     }


    /**
     * @OA\post(path="/admin/doctor/update",
     *     tags={"Doctor CRUD"},
     *     summary="Update a doctor",
     *     security={{"apiAuth":{}}},
     *     operationId="Updatedoctor",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="user id to update It",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CrearteDoctorByAdminRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param UpdateDoctorRequest $request
     * @return string
     */

    public function update_doctor(UpdateDoctorRequest $request)
    {
       return AdminDoctorServices::createOrUpdate($request);
    }



    /**
     * @OA\Get(path="/admin/doctor/get-all",
     *     tags={"Doctor CRUD"},
     *     summary="Get doctors",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function get_doctors()
    {
        return AdminDoctorServices::getDoctors();
    }



    /**
     * @OA\Get(path="/admin/doctor/get-one",
     *     tags={"Doctor CRUD"},
     *     summary="Get doctor",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="doctor_id",
     *         in="query",
     *         description="doctor Id To Show It",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function get_doctor(GetDoctorRequest $request)
    {
        return AdminDoctorServices::getDoctor($request);

    }

    /**
     * @OA\Delete(path="/admin/doctor/delete",
     *     tags={"Doctor CRUD"},
     *     summary="delete a doctor",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="doctor_id",
     *         in="query",
     *         description="doctor Id To Delete It",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param GetDoctorRequest $request
     * @return JsonResponse
     */

    public function delete_doctor(GetDoctorRequest $request)
    {
        return AdminDoctorServices::deleteDoctor($request);
    }
}
