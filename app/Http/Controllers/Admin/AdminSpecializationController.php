<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Specialization\CreateSpecializationRequest;
use App\Http\Requests\Specialization\GetSpecializationRequest;
use App\Http\Requests\Specialization\UpdateSpecializationRequest;
use App\services\AdminSpecializationServices;

class AdminSpecializationController extends Controller
{


    /**
     * @OA\post(path="/admin/specialization/create",
     *     tags={"Specialization CRUD"},
     *     summary="create a specialization",
     *     security={{"apiAuth":{}}},
     *     operationId="Creratespecialization",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CrearteSpecializationRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param CreateSpecializationRequest $request
     * @return string
     */

     public function create_specialization(CreateSpecializationRequest $request)
     {
        return AdminSpecializationServices::createOrUpdate($request);
     }


    /**
     * @OA\post(path="/admin/specialization/update",
     *     tags={"Specialization CRUD"},
     *     summary="Update a specialization",
     *     security={{"apiAuth":{}}},
     *     operationId="Updatespecialization",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="specialization id to update It",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CrearteSpecializationRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param UpdateSpecializationRequest $request
     * @return string
     */

    public function update_specialization(UpdateSpecializationRequest $request)
    {
       return AdminSpecializationServices::createOrUpdate($request);
    }



    /**
     * @OA\Get(path="/admin/specialization/get-all",
     *     tags={"Specialization CRUD"},
     *     summary="Get specializations",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function get_specializations()
    {
        return AdminSpecializationServices::getSpecializations();
    }



    /**
     * @OA\Get(path="/admin/specialization/get-one",
     *     tags={"Specialization CRUD"},
     *     summary="Get specialization",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="specialization_id",
     *         in="query",
     *         description="specialization Id To Show It",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function get_specialization(GetSpecializationRequest $request)
    {
        return AdminSpecializationServices::getSpecialization($request);

    }

    /**
     * @OA\Delete(path="/admin/specialization/delete",
     *     tags={"Specialization CRUD"},
     *     summary="delete a specialization",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="specialization_id",
     *         in="query",
     *         description="specialization Id To Delete It",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param GetSpecializationRequest $request
     * @return JsonResponse
     */

    public function delete_specialization(GetSpecializationRequest $request)
    {
        return AdminSpecializationServices::deleteSpecialization($request);
    }
}
