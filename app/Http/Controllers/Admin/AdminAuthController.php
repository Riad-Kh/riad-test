<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\AdminLoginRequest;
use App\services\AuthAdminServices;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\UpdateAdminProfileRequest;
use App\Http\Requests\Auth\UpdateProfileRequest;
use Illuminate\Http\Request;

class AdminAuthController extends Controller
{


    /**
     * @OA\Post(path="/admin/login",
     *     tags={"Auth Admin"},
     *     summary="Login as a admin",
     *     operationId="AdminAuthLogin",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/AdminLoginModel")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "User Profile response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param AdminLoginRequest $request
     * @return JsonResponse
     */
    public function login(AdminLoginRequest $request)
    {

      return AuthAdminServices::login($request);

    }

    /**
     * @OA\Post(path="/admin/logout",
     *     tags={"Auth Admin"},
     *     summary="Log out from system",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        return AuthAdminServices::Admin_logut($request);
    }

      /**
     * @OA\Post(path="/admin/update-profile",
     *     tags={"Auth Admin"},
     *     summary="Edit profile content",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *              @OA\Schema(ref="#components/schemas/EditProfileRequest"),
     *        )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResultProfileResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param UpdateAdminProfileRequest $request
     * @return JsonResponse
     */

    public function update_profile(UpdateAdminProfileRequest $request)
    {
        return AuthAdminServices::Update_Profile($request);
    }


    /**
     * @OA\Get(path="/admin/profile",
     *     tags={"Auth Admin"},
     *     summary="Get profile details",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function profile()
    {
        return AuthAdminServices::Admin_Profile();
    }


    /**
     * @OA\Delete(path="/admin/delete-account",
     *     tags={"Auth Admin"},
     *     summary="Delete as a admin",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @return JsonResponse
     */

    public function delete_account(Request $request)
    {
        return AuthAdminServices::DeleteAccount($request);
    }

    /**
     * @OA\Post(path="/admin/change-password",
     *     tags={"Auth Admin"},
     *     summary="Edit profile password",
     *     security={{"apiAuth":{}}},
     *     @OA\RequestBody(
     *         description="ChangePasswordPayload model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ChangePasswordPayload")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     */
    public function change_password(ChangePasswordRequest $request)

    {
        return AuthAdminServices::UserChangePassword($request);
    }

     /**
     * @OA\Get(path="/admin/check-token",
     *    tags={"Auth Admin"},
     *     summary="Test a token for expiration",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function check_token()
    {
        return AuthAdminServices::checkToken();

    }
}
