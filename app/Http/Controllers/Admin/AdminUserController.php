<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UpdateProfileByAdminRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\GetUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\users\CrearteUserByAdminRequest;
use App\services\AdminUserServices;


class AdminUserController extends Controller
{


    /**
     * @OA\post(path="/admin/user/create",
     *     tags={"User CRUD"},
     *     summary="create a user",
     *     security={{"apiAuth":{}}},
     *     operationId="CrerateUser",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CrearteUserByAdminRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param UpdateProfileByAdminRequest $request
     * @return string
     */

     public function create_user(CreateUserRequest $request)
     {
        return AdminUserServices::createOrUpdate($request);
     }


    /**
     * @OA\post(path="/admin/user/update",
     *     tags={"User CRUD"},
     *     summary="Update a user",
     *     security={{"apiAuth":{}}},
     *     operationId="Updateuser",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="user id to update It",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CrearteUserByAdminRequest")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param CrearteUserByAdminRequest $request
     * @return string
     */

    public function update_user(UpdateUserRequest $request)
    {
       return AdminUserServices::createOrUpdate($request);
    }



    /**
     * @OA\Get(path="/admin/user/get-all",
     *     tags={"User CRUD"},
     *     summary="Get users",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function get_users()
    {
        return AdminUserServices::getUsers();
    }



    /**
     * @OA\Get(path="/admin/user/get-one",
     *     tags={"User CRUD"},
     *     summary="Get user",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="User Id To Show It",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function get_user(GetUserRequest $request)
    {
        return AdminUserServices::getUser($request);

    }

    /**
     * @OA\Delete(path="/admin/user/delete",
     *     tags={"User CRUD"},
     *     summary="delete a user",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="User Id To Delete It",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param GetUserRequest $request
     * @return JsonResponse
     */

    public function delete_user(GetUserRequest $request)
    {
        return AdminUserServices::deleteUser($request);
    }
}
