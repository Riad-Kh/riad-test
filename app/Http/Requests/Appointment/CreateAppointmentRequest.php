<?php

namespace App\Http\Requests\Appointment;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateAppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'user_id' => ['required','exists:users,id'],
            'doctor_id' => ['required','exists:doctors,id'],
            'date' => ['required','date'],
            'time' => ['required','date_format:H:i:s'],
            'status' => ['required','in:1,2,3,4']
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(returnError($validator->errors()->all()));
    }
}
