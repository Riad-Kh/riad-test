<?php

namespace App\Http\Requests\Doctor;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id' => ['required' , 'exists:doctors,id'],
            'name' => ['nullable','string'],
            'phone' => ['nullable','numeric' , 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            'email' => ['nullable' , 'email' ,'unique:doctors'],
            'password' => ['nullable','min:8'],
            'specialization_id' => ['nullable' , 'exists:specializations,id']
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(returnError($validator->errors()->all()));
    }
}
