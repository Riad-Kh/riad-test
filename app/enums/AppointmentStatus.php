<?php

namespace App\enums;

class AppointmentStatus extends PhpEnum {

    const pendding = 1;
    const accepted = 2;
    const rejected = 3;
    const canceled = 4;
    const expired = 5;

    public static function Labels() {
        return [
            self::pendding => trans('Pendding'),
            self::accepted => trans('Accepted'),
            self::rejected => trans('Rejected'),
            self::canceled => trans('Canceled'),
            self::expired => trans('Expired'),
        ];
    }


}
