<?php

namespace App\enums;

class NotificationType extends PhpEnum
{
    const NEW_COMPANY_REGISTER= 1;
    const SENDBYADMIN = 3;
    const User_Approval = 4;



    public static function Labels($lang = null)
    {
        return [
            self::NEW_COMPANY_REGISTER => trans( 'New Company Has Registerd'),
            self::SENDBYADMIN => trans('Send By Admin'),
            self::User_Approval => trans('Good News! Approval reaches out Now'),

        ];
    }

    public static function UserNotifications($lang = null)
    {
        return [
            self::NEW_COMPANY_REGISTER => trans( 'New Company Has Registerd'),
            self::SENDBYADMIN => trans('Send By Admin'),
            self::User_Approval => trans('Good News! Approval reaches out Now'),

        ];
    }

    public static function AdminNotifications($lang = null)
    {
        return [
            self::NEW_COMPANY_REGISTER => trans('New Company Has Registerd'),

        ];
    }


}
