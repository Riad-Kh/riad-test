<?php

namespace App\Models\auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class EditProfileRequest
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="EditProfileRequest model",
 *     description="EditProfileRequest model",
 * )
 */
class EditProfileRequest extends Model
{
   /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;




    /**
     * @OA\Property(
     *     description="phone",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *     description="email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;
}
