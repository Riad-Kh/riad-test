<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\Request;
use Illuminate\Database\Eloquent\Model;


/**
 * Class LoginModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="LoginModel model",
 *     description="LoginModel model",
 * )
 */
class LoginModel extends Model
{
    /**
     * @OA\Property(
     *     description="Phone",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *     description="password",
     *     title="password",
     * )
     *
     * @var string
     */
    public $password;

    }

