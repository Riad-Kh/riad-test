<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\Request;
use Illuminate\Database\Eloquent\Model;


/**
 * Class RegisterUserModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="RegisterUserModel model",
 *     description="RegisterUserModel model",
 * )
 */

class RegisterUserModel extends Model
{

    /**
     * @OA\Property(
     *     description="First Name",
     *     title="first_name",
     * )
     *
     * @var string
     */
    public $first_name;

    /**
     * @OA\Property(
     *     description="Last Name",
     *     title="last_name",
     * )
     *
     * @var string
     */
    public $last_name;

    /**
     * @OA\Property(
     *     description="Phone",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;


    /**
     * @OA\Property(
     *     description="Email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     description="Password",
     *     title="password",
     * )
     *
     * @var string
     */
    public $password;

}

