<?php

namespace App\Models\Request;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class CrearteAppointmentRequest
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CrearteAppointmentRequest model",
 *     description="CrearteAppointmentRequest model",
 * )
 */
class CrearteAppointmentRequest extends Model
{
    /**
     * @OA\Property(
     *     description="user id",
     *     title="user_id",
     * )
     *
     * @var integer
     */
    public $user_id;


    /**
     * @OA\Property(
     *     description="doctor id",
     *     title="doctor_id",
     * )
     *
     * @var integer
     */
    public $doctor_id;


    /**
     * @OA\Property(
     *     description="date",
     *     title="date",
     * )
     *
     * @var date
     */
    public $date;

    /**
     * @OA\Property(
     *     description="time",
     *     title="time",
     * )
     *
     * @var timestamp
     */
    public $time;

    /**
     * @OA\Property(
     *     description="Activate - Dectivate Property, 1 => Pending, 2 => Accepted , 3  => Rejected , 4 => Canceld",
     *     enum={"1", "2" , "3" , "4"},
     *     title="status",
     * )
     *
     * @var integer
     */
    public $status;



}
