<?php

namespace App\Models\Request;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class UserCrearteAppointmentRequest
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="UserCrearteAppointmentRequest model",
 *     description="UserCrearteAppointmentRequest model",
 * )
 */
class UserCrearteAppointmentRequest extends Model
{

    /**
     * @OA\Property(
     *     description="doctor id",
     *     title="doctor_id",
     * )
     *
     * @var integer
     */
    public $doctor_id;


    /**
     * @OA\Property(
     *     description="date",
     *     title="date",
     * )
     *
     * @var date
     */
    public $date;

    /**
     * @OA\Property(
     *     description="time",
     *     title="time",
     * )
     *
     * @var timestamp
     */
    public $time;

}
