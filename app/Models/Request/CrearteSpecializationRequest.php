<?php

namespace App\Models\Request;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class CrearteSpecializationRequest
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CrearteSpecializationRequest model",
 *     description="CrearteSpecializationRequest model",
 * )
 */
class CrearteSpecializationRequest extends Model
{
   /**
     * @OA\Property(
     *     description=" Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;

}
