<?php

namespace App\Models\Request;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class FilterRequest
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="FilterRequest model",
 *     description="FilterRequest model",
 * )
 */
class FilterRequest extends Model
{
    /**
     * @OA\Property(
     *     description="user name",
     *     title="user_name",
     * )
     *
     * @var string
     */
    public $user_name;


    /**
     * @OA\Property(
     *     description="doctor name",
     *     title="doctor_name",
     * )
     *
     * @var string
     */
    public $doctor_name;


    /**
     * @OA\Property(
     *     description="specialization",
     *     title="specialization",
     * )
     *
     * @var string
     */
    public $specialization;


    /**
     * @OA\Property(
     *     description="Activate - Dectivate Property, 1 => Pending, 2 => Accepted , 3  => Rejected , 4 => Canceld",
     *     enum={"1", "2" , "3" , "4"},
     *     title="status",
     * )
     *
     * @var integer
     */
    public $status;



}
