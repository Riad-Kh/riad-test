<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\Request;
use Illuminate\Database\Eloquent\Model;


/**
 * Class AdminLoginModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="AdminLoginModel model",
 *     description="AdminLoginModel model",
 * )
 */
class AdminLoginModel extends Model
{
    /**
     * @OA\Property(
     *     description="Phone",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     description="password",
     *     title="password",
     * )
     *
     * @var string
     */
    public $password;

    }

