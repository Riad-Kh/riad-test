<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\Result;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SpecializationResult
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="SpecializationResult model",
 *     description="SpecializationResult model",
 * )
 */
class SpecializationResult extends Model
{
    protected $fillable = [
        'id' , 'name'

    ];

    /**
     * @OA\Property(
     *     description="ID",
     *     title="id",
     * )
     *
     * @var integer
     */
    public $id;


    /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;





}

