<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\Result;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AppointmentResult
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="AppointmentResult model",
 *     description="AppointmentResult model",
 * )
 */
class AppointmentResult extends Model
{
    protected $fillable = [
        'id' , 'user' , 'doctor' , 'status' , 'date' , 'time'

    ];

    /**
     * @OA\Property(
     *     description="ID",
     *     title="id",
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *     description="user",
     *     title="user",
     * )
     *
     * @var UserProfileResult
     */

     public $user;



    /**
     * @OA\Property(
     *     description="doctor",
     *     title="doctor",
     * )
     *
     * @var DoctorProfileResult
     */


    /**
     * @OA\Property(
     *     description="date",
     *     title="date",
     * )
     *
     * @var date
     */
    public $date;

     /**
     * @OA\Property(
     *     description="time",
     *     title="time",
     * )
     *
     * @var time
     */
    public $time;

    /**
     * @OA\Property(
     *     description="Status",
     *     title="status",
     * )
     *
     * @var IdValueApiModel
     */

     public $status;


}

