<?php

/**
 * @license Apache 2.0
 */


namespace App\Models\Result;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Result
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="Result model",
 *     description="Result model",
 * )
 */

class LoginResultModel extends Model
{
    protected $fillable = ['profile', 'token'];


    /**
     * @OA\Property(
     *     description="Profile data",
     *     title="profile",
     * )
     *
     * @var UserProfileResult
     */
    public $profile;

    /**
     * @OA\Property(
     *     description="token",
     *     title="message",
     * )
     *
     * @var string
     */
    public $token;

}
