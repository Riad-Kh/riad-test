<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\Result;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminProfileResult
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="AdminProfileResult model",
 *     description="AdminProfileResult model",
 * )
 */
class DoctorProfileResult extends Model
{
    protected $fillable = [
        'id' , 'name' , 'phone' , 'email' , 'specialization'

    ];

    /**
     * @OA\Property(
     *     description="ID",
     *     title="id",
     * )
     *
     * @var integer
     */
    public $id;


    /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;


    /**
     * @OA\Property(
     *     description="Phone",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;

     /**
     * @OA\Property(
     *     description="email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     description="Specialization",
     *     title="specialization",
     * )
     *
     * @var SpecializationResult
     */

     public $specialization;


}

