<?php

namespace App\Models\users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class CrearteDoctorByAdminRequest
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CrearteDoctorByAdminRequest model",
 *     description="CrearteDoctorByAdminRequest model",
 * )
 */
class CrearteDoctorByAdminRequest extends Model
{
   /**
     * @OA\Property(
     *     description=" Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     description="phone",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *     description="email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     description="password",
     *     title="password",
     * )
     *
     * @var string
     */
    public $password;


    /**
     * @OA\Property(
     *     description="specialization",
     *     title="specialization_id",
     * )
     *
     * @var integer
     */
    public $specialization_id;




}
