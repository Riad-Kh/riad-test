<?php

namespace App\Models\users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class CrearteUserByAdminRequest
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CrearteUserByAdminRequest model",
 *     description="CrearteUserByAdminRequest model",
 * )
 */
class CrearteUserByAdminRequest extends Model
{
   /**
     * @OA\Property(
     *     description=" Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     description="phone",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *     description="email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     description="password",
     *     title="password",
     * )
     *
     * @var string
     */
    public $password;


}
