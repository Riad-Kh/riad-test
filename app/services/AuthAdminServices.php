<?php

namespace App\services;
use App\utils\util;
use App\enums\LoginApiEnum;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Admin;
use App\Models\auth\AdminPermissions;
use App\Models\Result\ResultModel;
use Illuminate\Support\Facades\Auth;
use App\services\FillApiModels;
use App\Models\Result\LoginResultModel;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth as FacadesJWTAuth;
use Illuminate\Support\Collection;
class AuthAdminServices
{
    const Error_Message = "Some Thing is Wrong";
    const Login_Successfully_Message= " Loged in Successfully";
    const Registration_Success_Message = "Msg_RegistrationSuccess";
    const Successfully = "Successfully";

    public static function login ($request)
    {

        try{
            $login_model = new LoginResultModel();
            $token = Auth::guard('admin-api')->attempt(['email' => $request->email , 'password' => $request->password]);
            $admin = Auth::guard('admin-api')->user();
            if (!$admin)
                return returnError(LoginApiEnum::LabelOf(LoginApiEnum::invalid_credentials));

               $login_model['profile'] = FillApiModelService::FillAdminProfileModel($admin);
               $login_model['token'] = $token;


               return returnData(ResultModel::class , $login_model ,self::Login_Successfully_Message);

          }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
          }


    }

    public static function Create_Admin($request)
    {
        try {

            $user = User::query()->create($request->validated());
           return self::User_Login($request);
            // return returnSuccess(AuthServices::Registration_Success_Message);

        } catch (\Exception $ex) {

            return [false,null, AuthServices::Error_Message, $ex->getMessage()];
        }

    }

    public static function Admin_logut($request)
    {

        try {
            $token = Str::after($request->header('Authorization'), 'Bearer ');
            FacadesJWTAuth::setToken($token)->invalidate();
            return returnSuccess("Logged out successfully");
        } catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


    public static function Update_Profile($request)
    {
        try {

            $filterRequest = array_filter($request->validated());
            $model = admin();
            if ($model) {


                $model->update($filterRequest);


                return (!$model->save()) ? returnError("Error saving Admin") : returnSuccess("Admin has been updated");
            } else {
                return returnError("Admin not found");
            }

        } catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), 555);
        }
    }

    public static function Admin_Profile()
    {
        try {
                $admin = admin();

                $data = FillApiModelService::FillAdminProfileModel($admin);

                return returnData(ResultModel::class , $data ,self::Successfully);

        }catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function Admin_Permissions()
    {
        try {

             $role =admin()->role;
             $permissions = $role->permissions;
             $permission_categories =[];
             foreach ($permissions as $permission)
              {
                 $name =explode( '.', $permission->name )  ;

                 $permission_categories []= $name[1];
              }
              $permission_categories = array_unique($permission_categories);


              foreach($permission_categories as $name => $title) {
                  $data[] = FillApiModelService::FillMyPermissionsModel($permissions,$title);

              }
              $admin_permissions = new AdminPermissions([
                'role' =>  FillApiModelService::FillOneRole($role) ,
                'permissions' => $data
              ]);


                $result = new ResultModel([
                 'isOk'  => true,
                 'result' => $admin_permissions,
                 'message' => AuthServices::Successfully,

                ]);
                return [true, $result, '', ''];



        }catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }



    public static function DeleteAccount($request)
    {
        try {
            $model = admin();


            if ($model) {
                $token = Str::after($request->header('Authorization'), 'Bearer ');
                FacadesJWTAuth::setToken($token)->invalidate();

                $model->delete();
            } else {
                return returnError("User not found");
            }
            return returnSuccess("Deleted");

        } catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function UserChangePassword($request) {

        try {
            $admin = admin();

            if (!$admin)
                return returnError("admin not found");

            if(!Hash::check($request->oldPassword , $admin->password))
                return returnError("Wrong old password");


            $admin->password = Hash::make($request->newPassword);

            if (!$admin->save())
                return returnError("Error saving password");


            return returnSuccess('Password Changed');
        }catch (\Exception $ex){
            return returnError(AuthServices::Error_Message , $ex->getMessage() , $ex->getCode());
        }
    }

    public static function checkToken()
    {
        if (!\admin()) {
            return returnError(trans('Token expired'));
        }

        return returnSuccess(trans('Token Valid'));
    }


    public static function Create_Role($request){
        try {

            $data = Role::query()->updateOrCreate(['id' => $request['id'] ?? null] , $request);

        }catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message , $ex->getMessage() , $ex->getCode());
        }

    }


}
