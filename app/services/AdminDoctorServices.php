<?php

namespace App\services;

use App\Models\Doctor;
use App\Models\Result\ResultModel;

class AdminDoctorServices
{
    const Error_Message = "Some Thing is Wrong";

    const Successfully = "Successfully";


    public static function createOrUpdate($request)
    {
        try {


            $request = array_filter($request->validated());

            $doctor = Doctor::query()->updateOrCreate(['id' => $request['id'] ?? null] , $request);


                $data =FillApiModelService::FillDoctorProfileModel($doctor);

                return returnData(ResultModel::class , $data ,self::Successfully);

        }catch (\Exception $ex) {
            return returnError(self::Error_Message , $ex->getMessage() , $ex->getCode());
        }

    }


    public static function getDoctors()
    {
        try {


            $doctors = Doctor::query()->latest()->get();

                $data=[];
                foreach($doctors as $doctor)
                {
                    $data[]= FillApiModelService::FillDoctorProfileModel($doctor);
                }

               return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function getDoctor($request)
    {
        try {

            $doctor = Doctor::query()->where('id',$request->doctor_id)->first();


                $data= FillApiModelService::FillDoctorProfileModel($doctor);

                return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


    public static function deleteDoctor($request)
    {
        try {

            $doctor = Doctor::query()->findOrFail($request->doctor_id)->delete();

            return   returnSuccess("Done");

        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }



}
