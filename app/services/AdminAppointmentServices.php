<?php

namespace App\services;

use App\Models\Appointment;
use App\Models\Result\ResultModel;

class AdminAppointmentServices
{
    const Error_Message = "Some Thing is Wrong";

    const Successfully = "Successfully";


    public static function createOrUpdate($request)
    {
        try {


            $request = array_filter($request->validated());

            $appointment = Appointment::query()->updateOrCreate(['id' => $request['id'] ?? null] , $request);


                $data =FillApiModelService::FillAppointmentModel($appointment);

                return returnData(ResultModel::class , $data ,self::Successfully);

        }catch (\Exception $ex) {
            return returnError(self::Error_Message , $ex->getMessage() , $ex->getCode());
        }

    }


    public static function getAppointments()
    {
        try {


            $appointments = Appointment::query()->latest()->get();

                $data=[];
                foreach($appointments as $appointment)
                {
                    $data[]= FillApiModelService::FillAppointmentModel($appointment);
                }

               return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function getAppointment($request)
    {
        try {

            $appointment = Appointment::query()->where('id',$request->appointment_id)->first();


                $data= FillApiModelService::FillAppointmentModel($appointment);

                return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


    public static function deleteAppointment($request)
    {
        try {

            $appointment = Appointment::query()->findOrFail($request->appointment_id)->delete();

            return   returnSuccess("Done");

        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }



    public static function getUserAppointments($request)
    {
        try {


            $appointments = Appointment::query()->where('user_id',$request->user_id)->get();

                $data=[];
                foreach($appointments as $appointment)
                {
                    $data[]= FillApiModelService::FillAppointmentModel($appointment);
                }

               return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function filters($request)
    {
        try {


            $query = Appointment::query();
            if ($request->has('status')) {
                $query->where('status', $request->status);
            }

            if ($request->has('user_name')) {
                $query->whereHas('user', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->user_name . '%');
                });
            }

            if ($request->has('doctor_name')) {
                $query->whereHas('doctor', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->doctor_name . '%');
                });
            }

            if ($request->has('specialization')) {
                $query->whereHas('doctor.specialization', function ($q) use ($request) {
                    $q->where('name',  'like', '%' . $request->specialization . '%');
                });
            }

            $appointments = $query->get();
                $data=[];
                foreach($appointments as $appointment)
                {
                    $data[]= FillApiModelService::FillAppointmentModel($appointment);
                }

               return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

}
