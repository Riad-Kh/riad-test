<?php

namespace App\services;

use App\enums\AppointmentStatus;
use App\Models\auth\AdminPermissions;
use App\Models\permission\OnePermissionResultModel;
use App\Models\permission\PermissionResultModel;
use App\Models\permission\RoleResultModel;
use App\Models\Result\AdminProfileResult;
use App\Models\Result\AppointmentResult;
use App\Models\Result\DoctorProfileResult;
use App\Models\Result\SpecializationResult;
use App\Models\Result\UserProfileResult;

class FillApiModelService
{

    public static function FillUserProfileModel($item)
    {
        return new UserProfileResult([
            'id' => $item->id,
            'name' => $item->name,
            'phone' => $item->phone,
            'email' => $item->email,
        ]);
    }

    public static function FillAdminProfileModel($item)
    {
        return new AdminProfileResult([
            'id' => $item->id,
            'name' => $item->name,
            'phone' => $item->phone,
            'email' => $item->email,
        ]);
    }


    public static function FillDoctorProfileModel($item)
    {
        return new DoctorProfileResult([
            'id' => $item->id,
            'name' => $item->name,
            'phone' => $item->phone,
            'email' => $item->email,
            'specialization' => self::FillSpecializationModel($item->specialization),
        ]);
    }

    public static function FillSpecializationModel($item)
    {
        return new SpecializationResult([
            'id' => $item->id,
            'name' => $item->name,
        ]);
    }

    public static function FillAppointmentModel($item)
    {
        return new AppointmentResult([
            'id' => $item->id,
            'date' => $item->date,
            'time' => $item->time,
            'doctor' => self::FillDoctorProfileModel($item->doctor),
            'user' => self::FillUserProfileModel($item->user),
            'status' => FillIdValueApiModel($item->status,AppointmentStatus::LabelOf($item->status)),
        ]);
    }

    public static function FillRolesModel($items)
    {
        $roles = [];
        foreach ($items as $one){
            $roles[] = self::FillOneRole($one);
        }
        return $roles;
    }

    public static function FillOneRole($item)
    {
        return new RoleResultModel([
            'id' => $item->id,
            'name' => $item->name,
        ]);
    }

    public static function FillPermissionsModel($items,$title)
    {
            $permission =new PermissionResultModel([
                'table' => $title,
                'permissions' =>  self::FillPermissions($items),
            ]);

        return $permission;
    }

    public static function FillPermissions($item)
    {
        $permission = [];
            foreach ($item as $one){
                $permission [] =  new OnePermissionResultModel([
                    'id' => $one->id,
                    'name' => $one->name,
                ]);
            }
        return $permission;
    }


    public static function FillMyPermissionsModel($items,$title)
    {
            return new PermissionResultModel([
                'table' => $title,
                'permissions' =>  FillPermission($title ,$items),
            ]);


    }

}
