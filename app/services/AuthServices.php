<?php

namespace App\services;

use App\enums\LoginApiEnum;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Result\ResultModel;
use Illuminate\Support\Facades\Auth;
use App\services\FillApiModels;
use App\Models\Result\LoginResultModel;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth as FacadesJWTAuth;

class AuthServices
{
    const Error_Message = "Some Thing is Wrong";
    const Login_Successfully_Message= " Loged in Successfully";
    const Registration_Success_Message = "Msg_RegistrationSuccess";
    const Successfully = "Successfully";

    public static function User_Login ($request)
    {

        try{
            $login_model = new LoginResultModel();
            $token = Auth::guard('user-api')->attempt(['phone' => $request->phone , 'password' => $request->password]);
            $user = Auth::guard('user-api')->user();
            if (!$user)
                return  [false,null, LoginApiEnum::LabelOf(LoginApiEnum::invalid_password), ''];

            if($user)
            {
               $login_model['profile'] = FillApiModelService::FillUserProfileModel($user);
               $login_model['token'] = $token;

               $result = new ResultModel([
                'isOk'  => true,
                'result' => $login_model,
                'message' => AuthServices::Login_Successfully_Message,

               ]);
               return [true, $result, '', ''];
            }

          }catch (\Exception $ex) {
            return [false,null, AuthServices::Error_Message, $ex->getMessage()];
          }


    }

    public static function User_Register($request)
    {
        try {

            $user = User::query()->create($request->validated());
           return self::User_Login($request);
            // return returnSuccess(AuthServices::Registration_Success_Message);

        } catch (\Exception $ex) {

            return [false,null, AuthServices::Error_Message, $ex->getMessage()];
        }

    }

    public static function User_logut($request)
    {

        try {
            $token = Str::after($request->header('Authorization'), 'Bearer ');
            FacadesJWTAuth::setToken($token)->invalidate();
            return returnSuccess("Logged out successfully");
        } catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


    public static function Update_Profile($request)
    {
        try {

           $filterRequest = array_filter($request->validated());
            $model = user();
            if ($model) {

                if ($request->image) {
                    if($model->image != 'users/default.png')
                    Storage::disk('public')->delete($model->image);
                     $filterRequest['image'] = uploadImage($request->image, User::image_directory);
                }

                $model->update($filterRequest);


                return (!$model->save()) ? returnError("Error saving user") : returnSuccess("User has been updated");
            } else {
                return returnError("User not found");
            }

        } catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), 555);
        }
    }

    public static function User_Profile()
    {
        try {
            $user = user();
            if($user)
            {
               $result = new ResultModel([
                'isOk'  => true,
                'result' => FillApiModelService::FillUserProfileModel($user),
                'message' => AuthServices::Successfully,

               ]);
               return [true, $result, '', ''];
            }
        }catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function DeleteAccount($request)
    {
        try {
            $model = user();


            if ($model) {
                $token = Str::after($request->header('Authorization'), 'Bearer ');
                FacadesJWTAuth::setToken($token)->invalidate();

                $model->delete();
            } else {
                return returnError("User not found");
            }
            return returnSuccess("Deleted");

        } catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function UserChangePassword($request) {

        try {
            $user = user();

            if (!$user)
                return returnError("User not found");

            if(!Hash::check($request->oldPassword , $user->password))
                return returnError("Wrong old password");


            $user->password = Hash::make($request->newPassword);

            if (!$user->save())
                return returnError("Error saving password");


            return returnSuccess('Password Changed');
        }catch (\Exception $ex){
            return returnError(AuthServices::Error_Message , $ex->getMessage() , $ex->getCode());
        }
    }

    public static function checkToken()
    {
        if (!\user()) {
            return returnError(trans('Token expired'));
        }

        return returnSuccess(trans('Token Valid'));
    }


}
