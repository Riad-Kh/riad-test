<?php

namespace App\services;

use App\enums\AppointmentStatus;
use App\enums\LoginApiEnum;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Result\ResultModel;
use Illuminate\Support\Facades\Auth;
use App\services\FillApiModels;
use App\Models\Result\LoginResultModel;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth as FacadesJWTAuth;

class DoctorServices
{
    const Error_Message = "Some Thing is Wrong";
    const Login_Successfully_Message= " Loged in Successfully";
    const Registration_Success_Message = "Msg_RegistrationSuccess";
    const Successfully = "Successfully";

    public static function login ($request)
    {

        try{
            $login_model = new LoginResultModel();
            $token = Auth::guard('doctor-api')->attempt(['phone' => $request->phone , 'password' => $request->password]);
            $doctor = Auth::guard('doctor-api')->user();
            if (!$doctor)
                return returnError(LoginApiEnum::LabelOf(LoginApiEnum::invalid_credentials));

               $login_model['profile'] = FillApiModelService::FillDoctorProfileModel($doctor);
               $login_model['token'] = $token;


               return returnData(ResultModel::class , $login_model ,self::Login_Successfully_Message);

          }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
          }


    }


    public static function accept_reject($request)
    {
        try {

            $appointment = doctor()->appointments()
                                 ->where('status' , AppointmentStatus::pendding)
                                 ->find($request->id);

                if(!$appointment)
                    return   returnError("Not Allowed");


            if ($request->status == 2) {
                $appointment->status = AppointmentStatus::accepted;
            } elseif ($request->status == 3) {
                $appointment->status = AppointmentStatus::rejected;
            }


            $appointment->save();

        return returnSuccess("Appointment status changed successfully.");

        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


}
