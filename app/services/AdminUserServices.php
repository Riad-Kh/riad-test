<?php

namespace App\services;

use App\Models\User;
use App\Models\Result\ResultModel;

class AdminUserServices
{
    const Error_Message = "Some Thing is Wrong";

    const Successfully = "Successfully";


    public static function createOrUpdate($request)
    {
        try {


            $request = array_filter($request->validated());

         return   $user = User::query()->updateOrCreate(['id' => $request['id'] ?? null] , $request);


                $data =FillApiModelService::FillUserProfileModel($user);

                return returnData(ResultModel::class , $data ,self::Successfully);

        }catch (\Exception $ex) {
            return returnError(self::Error_Message , $ex->getMessage() , $ex->getCode());
        }

    }


    public static function getUsers()
    {
        try {


            $users = User::query()->latest()->get();

                $data=[];
                foreach($users as $user)
                {
                    $data[]= FillApiModelService::FillUserProfileModel($user);
                }

               return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function getUser($request)
    {
        try {

            $user = User::query()->where('id',$request->user_id)->first();


                $data= FillApiModelService::FillUserProfileModel($user);

                return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


    public static function deleteUser($request)
    {
        try {

            $User = User::query()->findOrFail($request->user_id)->delete();

            return   returnSuccess("Done");

        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }



}
