<?php

namespace App\services;

use App\Models\Specialization;
use App\Models\Result\ResultModel;

class AdminSpecializationServices
{
    const Error_Message = "Some Thing is Wrong";

    const Successfully = "Successfully";


    public static function createOrUpdate($request)
    {
        try {


            $request = array_filter($request->validated());

            $specialization = Specialization::query()->updateOrCreate(['id' => $request['id'] ?? null] , $request);


                $data =FillApiModelService::FillSpecializationModel($specialization);

                return returnData(ResultModel::class , $data ,self::Successfully);

        }catch (\Exception $ex) {
            return returnError(self::Error_Message , $ex->getMessage() , $ex->getCode());
        }

    }


    public static function getSpecializations()
    {
        try {


            $specializations = Specialization::query()->latest()->get();

                $data=[];
                foreach($specializations as $specialization)
                {
                    $data[]= FillApiModelService::FillSpecializationModel($specialization);
                }

               return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function getSpecialization($request)
    {
        try {

            $specialization = Specialization::query()->where('id',$request->specialization_id)->first();


                $data= FillApiModelService::FillSpecializationModel($specialization);

                return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


    public static function deleteSpecialization($request)
    {
        try {

            $specialization = Specialization::query()->findOrFail($request->specialization_id)->delete();

            return   returnSuccess("Done");

        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }



}
