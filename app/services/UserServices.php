<?php

namespace App\services;

use App\enums\AppointmentStatus;
use App\Models\Appointment;
use App\Models\Result\ResultModel;

class UserServices
{
    const Error_Message = "Some Thing is Wrong";

    const Successfully = "Successfully";


    public static function create($request)
    {
        try {
            $request = array_filter($request->validated());

            $request['user_id'] = user()->id;

            $request['status'] = AppointmentStatus::pendding;

            $appointment = Appointment::query()->create( $request);


                $data =FillApiModelService::FillAppointmentModel($appointment);

                return returnData(ResultModel::class , $data ,self::Successfully);

        }catch (\Exception $ex) {
            return returnError(self::Error_Message , $ex->getMessage() , $ex->getCode());
        }

    }




    public static function myAppointments()
    {
        try {

            $appointments = user()->appointments;

                $data=[];
                foreach($appointments as $appointment)
                {
                    $data[]= FillApiModelService::FillAppointmentModel($appointment);
                }

               return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


    public static function cancel($request)
    {
        try {

            $appointment = user()->appointments()
                                 ->where('status' , AppointmentStatus::pendding)
                                 ->find($request->id);

              if(!$appointment)
                 return   returnError("Not Allowed");

                    $appointment->status = AppointmentStatus::canceled;
                    $appointment->save();

                return returnSuccess("Appintment Canceled Successfully");


        }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }




}
