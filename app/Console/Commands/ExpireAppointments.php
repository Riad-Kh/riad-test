<?php

namespace App\Console\Commands;

use App\enums\AppointmentStatus;
use App\Models\Appointment;
use Illuminate\Console\Command;
use Carbon\Carbon;

class ExpireAppointments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appointments:expire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire appointments that have passed the scheduled booking date';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $expiredAppointments = Appointment::where('status', AppointmentStatus::pendding)
            ->where(function ($query) {

                $query->where('date', '<', Carbon::today())
                    ->orWhere(function ($query) {
                        $query->whereDate('date', Carbon::today())
                            ->whereTime('time', '<', Carbon::now()->format('H:i:s'));
                    });
            })
            ->update(['status' => AppointmentStatus::expired]);

        $this->info('Expired appointments: ' . $expiredAppointments);
    }
}
