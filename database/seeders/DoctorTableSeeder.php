<?php

namespace Database\Seeders;

use App\Models\Doctor;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DoctorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Doctor::query()->create([
            'name'  => 'doctr',
            'phone' => '0987654321',
            'email' => 'doc@test.com',
            'password' => '12345678',
            'specialization_id' => 1
        ]);
    }
}
