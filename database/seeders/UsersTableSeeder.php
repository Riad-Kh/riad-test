<?php

namespace Database\Seeders;

use App\enums\ActiveInactiveStatus;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::query()->create([
            'name'  => 'hamdi',
            'phone' => '0987654321',
            'email' => 'user@test.com',
            'password' => '12345678',
        ]);
    }
}
