<?php

namespace Database\Seeders;

use App\enums\ActiveInactiveStatus;
use App\Models\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::query()->create([
            'name'   => 'admin',
            'phone' => '0987654321',
            'email' => 'admin@admin.com',
            'password' => '12345678',
        ]);
    }
}
