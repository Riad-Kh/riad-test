<?php

use App\Http\Controllers\Admin\AdminAppointmentController;
use App\Http\Controllers\Admin\AdminAuthController;
use App\Http\Controllers\Admin\AdminDoctorController;
use App\Http\Controllers\Admin\AdminSpecializationController;
use App\Http\Controllers\Admin\PermissionRoleController;
use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Doctor\DoctorController;
use App\Http\Controllers\User\AppointmentController;
use App\Http\Controllers\User\AuthController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Models\Permission as ModelsPermission;
use Spatie\Permission\Models\Role;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user-login', [AuthController::class,'user_login']);
Route::post('user-register', [AuthController::class,'user_register']);

Route::group(['middleware' => ['auth:user-api']] , function (){

    Route::get('profile', [AuthController::class,'profile']);
    Route::post('update-profile',[AuthController::class,'update_profile']);
    Route::post('logout' , [AuthController::class,'logout']);
    Route::delete('delete-account', [AuthController::class,'delete_account']);
    Route::post('change-password',[AuthController::class,'change_password']);
    Route::get('check-token',[AuthController::class,'check_token']);


});

                    ///////////////////////admin///////////////////////
Route::group(['prefix' => 'admin'] , function(){

    Route::post('login', [AdminAuthController::class,'login']);

    Route::group(['middleware' => ['auth:admin-api']] , function (){

        Route::get('profile', [AdminAuthController::class,'profile']);
        Route::post('update-profile',[AdminAuthController::class,'update_profile']);
        Route::post('logout' , [AdminAuthController::class,'logout']);
        Route::delete('delete-account', [AdminAuthController::class,'delete_account']);
        Route::post('change-password',[AdminAuthController::class,'change_password']);
        Route::get('check-token',[AdminAuthController::class,'check_token']);
    });

});


                 ///////////////////////User Crud///////////////////////


Route::group(['prefix' => 'admin/user'] , function(){
    Route::group(['middleware' => ['auth:admin-api']] , function (){
            Route::post('create', [AdminUserController::class,'create_user']);
            Route::post('update', [AdminUserController::class,'update_user']);
            Route::delete('delete', [AdminUserController::class,'delete_user']);
            Route::get('get-all',[AdminUserController::class,'get_users']);
            Route::get('get-one',[AdminUserController::class,'get_user']);
        });
});


                 ///////////////////////Doctor Crud///////////////////////


Route::group(['prefix' => 'admin/doctor'] , function(){
    Route::group(['middleware' => ['auth:admin-api']] , function (){
            Route::post('create', [AdminDoctorController::class,'create_doctor']);
            Route::post('update', [AdminDoctorController::class,'update_doctor']);
            Route::delete('delete', [AdminDoctorController::class,'delete_doctor']);
            Route::get('get-all',[AdminDoctorController::class,'get_doctors']);
            Route::get('get-one',[AdminDoctorController::class,'get_doctor']);
        });
});


                 ///////////////////////Specialization Crud///////////////////////


Route::group(['prefix' => 'admin/specialization'] , function(){
    Route::group(['middleware' => ['auth:admin-api']] , function (){
            Route::post('create', [AdminSpecializationController::class,'create_specialization']);
            Route::post('update', [AdminSpecializationController::class,'update_specialization']);
            Route::delete('delete', [AdminSpecializationController::class,'delete_specialization']);
            Route::get('get-all',[AdminSpecializationController::class,'get_specializations']);
            Route::get('get-one',[AdminSpecializationController::class,'get_specialization']);
        });
});


                 ///////////////////////Appointment Crud///////////////////////


Route::group(['prefix' => 'admin/appointment'] , function(){
    Route::group(['middleware' => ['auth:admin-api']] , function (){
            Route::post('create', [AdminAppointmentController::class,'create_appointment']);
            Route::post('update', [AdminAppointmentController::class,'update_appointment']);
            Route::delete('delete', [AdminAppointmentController::class,'delete_appointment']);
            Route::get('get-all',[AdminAppointmentController::class,'get_appointments']);
            Route::get('get-one',[AdminAppointmentController::class,'get_appointment']);
            Route::get('user-appointments',[AdminAppointmentController::class,'user_appointments']);
            Route::post('filters',[AdminAppointmentController::class,'filters']);
        });
});



                 ///////////////////////User///////////////////////


Route::group(['prefix' => 'user/appointment'] , function(){
    Route::group(['middleware' => ['auth:user-api']] , function (){
            Route::post('create', [AppointmentController::class,'create_appointment']);
            Route::get('my_appointment', [AppointmentController::class,'my_appointments']);
            Route::patch('cancel', [AppointmentController::class,'cancel']);

        });
});


                 ///////////////////////Doctor///////////////////////


Route::group(['prefix' => 'doctor'] , function(){
         Route::post('login', [DoctorController::class,'login']);

         Route::group(['middleware' => ['auth:doctor-api']]  , function (){

            Route::patch('accept-reject', [DoctorController::class,'accept_reject']);

        });
});

